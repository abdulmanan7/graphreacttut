var express = require('express');
var graphqlHTTP = require('express-graphql');
const schema = require('./schema/index');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();
app.use(cors());
mongoose.connect('mongodb://localhost:27017/gql-server',{ useNewUrlParser: true });
mongoose.connection.once('open',()=>{
    console.log('db connected');
});
app.use('/graphql',graphqlHTTP({
    schema,
    graphiql:true
}));
app.listen(4000,()=>{
    console.log("now listening for requests on port 4000");
});